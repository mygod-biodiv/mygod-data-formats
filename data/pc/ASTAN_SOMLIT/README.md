# ASTAN SOMLIT Physico-chemical Measurements Example Dataset

The files in this directory have been built from a dataset obtained from the [SOMLIT](https://www.somlit.fr/) French National Observation Service.

SOMLIT data can be requested at: https://www.somlit.fr/demande-de-donnees/

