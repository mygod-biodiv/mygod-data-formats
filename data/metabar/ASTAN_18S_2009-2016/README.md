# ASTAN 18S V4 2009-2016 MyGOD Metabarcode Example Dataset

The files in this directory have been built from a dataset provided by Nicolas Henry :


  >Nicolas Henry, Mariarita Caracciolo, Frédéric Mahé, Sarah Romac, Fabienne Rigaut-Jalabert, & Nathalie Simon. (2021). 18S V4 rDNA sequences organized at the OTU level for the SOMLIT-Astan time-series (2009-2016)

Available at (https://doi.org/10.5281/zenodo.5032450)


