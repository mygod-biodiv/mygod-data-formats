# Ocean Sampling Day 2018 16S MyGOD Metabarcode Example Dataset

The files in this directory have been built from files made available by the OSD2018 GitHub repository : https://github.com/ocean-sampling-day/OSD2018 

The MyGOD dataset has been formatted using files containing only 16S data. Since then the OSD2018 GitHub repository has been updated to include 18S data, which is thus not part of the MyGOD dataset.



