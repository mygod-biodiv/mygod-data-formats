# MyGOD Data Formats


## Table of Contents

1. [Supported Data Types](#mygod-supported-data-types)
2. [Metabarcode Data Files](#metabarcode-data-files)
3. [Metabarcode Event Files](#metabarcode-event-files)
4. [Metabarcode Sample Files](#metabarcode-sample-files)
5. [Metabarcode Table Configuration Files](#metabarcode-table-configuration-files)
6. [Metabarcode Dataset Description File](#metabarcode-dataset-description-file)
7. [Physico-chemical Data Files](#physico-chemical-data-files)
8. [Physico-chemical Data Table Configuration Files](#pysico-chemical-data-table-configuration-files)
9. [Default physico-chemical parameter descriptions](#default-physico-chemical-parameter-descriptions)
10. [Physico-chemical Data Files](#physico-chemical-data-files)
11. [Pysico-chemical Data Table Configuration Files](#pysico-chemical-data-table-configuration-files)
12. [Associating Physico-chemical measurements to Metabarcode datasets](#associating-physico-chemical-measurements-to-metabarcode-datasets)


## MyGOD Supported Data Types

The current version of MyGOD is cabable of handling metabarcode data as well as environmental physico-chemical data. 
Both types of data come in tabular format (a.k.a CSV-like files). To make sense of the tabular data, as well as to describe the dataset to be imported
MyGOD also needs two extra JSON-formatted files: a metadata file and a table configuration file.

## Metabarcode Data Files

MyGOD has been primarily designed to visualize metabarcode data. Metabarcode data comes in two separate files:

- An *event* file with information about the sampling events (sample identifier, sampling location, date/time, depth...). Each line describes a single sample.
- A *sample* file with information about the reads found in each sample (amplicon identifier, sequence, taxomic assignment, read count of the amplicon in each sample...). Each line describes a single amplicon.

The *sample identifier* is present in both tables to allow mapping reads and sampling data.

*Important notes*

  - When collecting metadata samples, a single sampling event can yield multiple samples. Typically, a single sampling event can be used to produce samples with various fraction sizes. Data producers often only provide sample identifiers where they encode the event identifier with other data (such as the fraction size). Thus, in order to be able to make a clear distinction between sampling events and samples, MyGOD needs to be able to extract event identifiers using the provided sample identifiers. This is done with a [Python regular expression](https://docs.python.org/3/howto/regex.html#regex-howto) as explained below.
  - In the examples below, the sample identifier is made of an event identifier followed by an underscore character and a fraction size (i.e. : the `RA20090605_02`sample identifier refers to the `02` fraction size of sampling event `RA20090605`), the matching regular expression to extract the event identifier as *capturing group* `event_id` will be : `^(?P<event_id>.*)_.*$`

### Metabarcode Event Files

The following table represents the typical structure of an event file describing a single sampling event:


  | sample_id | station_code | depth_code | depth_min | min_frac_size | date_time_utc_start | latitude_start | longitude_start |
  |-----------|--------------|------------|-----------|---------------|---------------------|----------------|-----------------|
  | RA20090605_02 | Roscoff Astan | Surface | 5 | 0.2 | 2009-06-05 | 48.7717 | -3.9683 |
  | RA20090703_02 | Roscoff Astan | Surface | 5 | 0.2 | 2009-07-03 | 48.7717 | -3.9683 |

The names and the order of the columns can be modified to suit the data producer's needs as long as the table structure is correctly described in the table configuration file (see below)

### Metabarcode Sample Files

The following table represents the typical structure of a sample file containing information about the reads identified in a set of samples.

  | amplicon | sequence | taxonomy | taxonomy_score | *RA20090605_02* | *RA20090703_02* | *sample_id 3* | ... | *sample_id n* |
  |----------|----------|----------|----------------|---------------|---------------|---------------|-----|---------------|
  | 120d6ee\(...\) | TACGGGAGGGG\(...\)| Bacteria\|Proteobacteria\|\(...\)\|Berkeleya_fennica | 75.3 | 0 | 40 | 629 | ... | 0 |

As for the event files, the names and the order of the columns are not fixed, the table structure being described in the table configuration file (see below)

*Important notes*
  - The `taxonomy` column contains a string concatenation of the whole taxonomic classification of the current amplicon, from the top-level to the leaf node. The pipe character \(`\|`\) is used to separate the taxon names at the different levels.
  - Columns with actual count values (starting with column `RA20090605_02` in the example) must have a header matching an entry of the `sample_id` column in the sampling event file.


### Metabarcode Table Configuration Files

This JSON-formatted file defines the organisation of the columns in the metabarcode data files (event file and sample file), as well as which column separator is used, and how to parse sample dates.

The file matching the above example would look like:

    {
      "SAMPLE_ID_INDEX":0,
      "STATION_CODE_INDEX":1,
      "DEPTH_CODE_INDEX":2,
      "DEPTH_MIN_INDEX":3,
      "DEPTH_MAX_INDEX":3,
      "FRACTION_SIZE_INDEX":4,
      "DATE_TIME_INDEX":5,
      "DATE_FORMAT" : "%Y-%m-%d",
      "LATITUDE_INDEX":6,
      "LONGITUDE_INDEX":7,
      "EVENT_DELIMITER":"\t",
      "AMPLICON_INDEX": 0,
      "SEQUENCE_INDEX": 1,
      "TAXONOMY_INDEX": 2,
      "IDENTITY_INDEX": 3,
      "EVENT_START_INDEX": 4,
      "EVENT_ID_REGEX": "^(?P<event_id>.*)_.*$",
      "COUNT_DELIMITER":"\t"
    }

Each entry with an `_INDEX` suffix is used to define which column of the sampling or metabarcode data file contains a specific piece of information. *Indices start at 0.*

#### The following entries are used to describe the organization of the *event* file:

- `SAMPLE_ID_INDEX`: index of column containing sample identifiers,
- `STATION_CODE_INDEX`: index of column containing station codes,
- `DEPTH_CODE_INDEX`: index of column containing depth codes,
- `DEPTH_MIN_INDEX`: index of column containing minimum sampling depth (m. below surface),
- `DEPTH_MAX_INDEX`: index of column containing maximum sampling depth (m. below surface, can be identical to `DEPTH_MIN_INDEX`),
- `FRACTION_SIZE_INDEX`: index of column describing the filter fraction size,
- `DATE_TIME_INDEX`: index of column containing the sampling date (and time),
- `DATE_TIME_FORMAT`: a string describing how the date and time are formatted in the `DATE_TIME_INDEX`column. Any format compatible with [Python's strftime() function](https://docs.python.org/3/library/datetime.html#strftime-and-strptime-behavior) can be used.
- `LATITUDE_INDEX`: index of column containing the latitude (WGS84, decimal format),
- `LONGITUDE_INDEX`: index of column containing the longitude (WGS84, decimal format),
- `EVENT_ID_REGEX`: a [Python regular expression](https://docs.python.org/3/howto/regex.html#regex-howto) defining how to extract an event identifier from the sample id column values.
- `EVENT_DELIMITER`: the character used in the event file to separate the columns.

#### The following entries are used to describe the organization of the *sample* file:

- `AMPLICON_INDEX`: index of column containing the amplicon identifiers,
- `SEQUENCE_INDEX`: index of column containing the sequence,
- `TAXONOMY_INDEX`: index of column containing the taxonomical classification of the current amplicon,
- `IDENTITY_INDEX`: index of column containing the percentage of sequecne identity,
- `EVENT_START_INDEX`: index of first column containing the count for each sampling event. All columns with an index equal or greater than the `EVENT_START_INDEX` will be considered as containing sampling event counts.
- `EVENT_ID_REGEX`: a [Python regular expression](https://docs.python.org/3/howto/regex.html#regex-howto) defining how to extract an event identifier from the sample id used as column header.
- `COUNT_DELIMITER`: the character used in the sample file to separate the columns.


### Metabarcode Dataset Description File

This JSON-formatted file provides metadata about the metabarcode dataset. For the above example, the contents of the file are as follows :

    {
    "name": "Astan 16S - 2009-2016 Example",
    "description": "Astan 16S V4 2009-2016 Example Dataset for MyGOD",
    "campaign": "Astan 2009-2016",
    "project": "Astan MyGOD Example",
    "owners" : [{"first_name":"Principal","last_name": "Investigator","contact_email" : "principal.investigator@mygod.org"}],
    "marker" : "V4",
    "site" : "Roscoff Astan Buoy",
    "fraction" : [{"0.2-3 µm" : "0.2"}, {">3 µm":"3"}],
    "minimal_identity":"90%",
    "taxonomy" : "silva",
    "version_taxonomy" : "22252",
    "rank_description": ["Domain", "Phylum", "Class","Order", "Family", "Genus", "Species"],
    "pc_parameter": [{"Temperature":"temperature"},
                     {"Dissolved Oxygen":"o"},
                     {"pH":"ph"},
                     {"Salinity":"s"},
                     {"Ammonium (NH4)": "nh4"},
                     {"Nitrate (NO3)": "no3"},
                     {"Nitrite (NO2)": "no2"},
                     {"Phosphate (PO4)": "po4"}
                     ],
    "datatype":"Temporal",
    "related_publications":[]
    }


With a brief explanation of each of the JSON dictionary entries:

- `name` : a short name for the dataset that will be used for display in tables, lists and summaries,
- `description` : a one sentence description of the dataset that gices some clues about its contents,
- `campaign` : if the dataset relates to a recurring sampling effort, a label for this specific sampling campaing can be defined in `campaign`, 
- `project` : if the dataset can is part of a collection of datasets belonging to a single project, ̀`project` can be used to name the... project,
- `owners`: the list of owners of a dataset. Each owner is described in a dictionary having the following entrires: `first_name`, `last_name` and `contact_email`,
- `marker`: the name of the genetic marker used for a metabarcode dataset,
- `site`: a label for the location where sampling was performed,
- `fraction`:  the list of dictionaries describing the fraction classes (or sizes) extracted from the sample, each dictionary contains a key with the label that defined the fraction class and will be displayed in the user interface, the value associated to the key will be the value that is contained in the data tables.
- `minimal_identity`: the minimal sequence identity percentage that was used when assigning a read to a reference sequence,
- `taxonomy`: the name of the taxonomical framework used for taxonomic assignment of the sequences,
- `version_taxonomy`: a version identifier defining which version of the taxonomical framework has been used for taxonomic assignment,
- `rank_description`: for fixed-rank taxonomies (such as Silva, or PR2), the labels that match each level in the taxonomy,
- `pc_parameter`: a list of dictionaries, each element of the list describes a physico-chemical parameter with a dictionary whose key is a human-readable label for the parameter, and the value is the actual attribute for that parameter stored in the database. The label will be displayed on graphical representations, the attribute is used in queries. A table below gives the possible values for the attributes.
- `datatype`: the datatype of the dataset; can be "Temporal", "Spatial" (or "Other" ?)
- `related_publications` : a list of items with information about publications related to this dataset (*format to be specified further*)
- `base_layers` : a list of dictionary entries, each element describing a base map layer to be made available for this dataset ; the elements must contain the following keys : "name" (the layer title), "url" : the entry point of the web service providing the map tiles (cf. for example [https://leaflet-extras.github.io/leaflet-providers/preview/](https://leaflet-extras.github.io/leaflet-providers/preview/)).


## Physico-chemical Data

MyGOD is able to associate physico-chemical measurements to metabarcode data. These measurements are stored in separate files. MyGOD defines a default list of physico-chemical parameters, to which custom parameters can be added. Queries relying on custom parameters however, will be slower that queries only using default parameters.

### Default physico-chemical parameter descriptions

The following table describes the default physico-chemical parameters :
- column 1 contains the name of the parameter (as stored in the Metabarcode Dataset description file).
- column 2 contains the name of the physico-chemical data table configuration entry with the column index for the parameter in the data table
- column 3 contains the name of the physico-chemical data table configuration entry with the parameter's unit.

| Parameter name | Parameter column index name | Parameter unit name | Attribute name |
|----------------|----------------------|---------------------|-----------------------|
| Ammonium       | `NH4_INDEX`          | `NH4_UNIT`          | `nh4` |
| Chlorophylle A | `CHLA_INDEX`         | `CHLA_UNIT`         | `chla`|
| Dissolved Oxygen | `OXYGEN_INDEX`     |  `OXYGEN_UNIT`      | `o`|
| Nitrate | `NO3_INDEX` | `NO3_UNIT` | `no3` |
| Nitrite | `NO2_INDEX` | `NO2_UNIT` | `no2`|
| pH  | `PH_INDEX` | N/A | `ph` |
| Phosphate | `PO4_INDEX` | `PO4_UNIT` | `po4` |
| Salinity | `SALINITY_INDEX` | `SALINITY_UNIT` | `s`|
| Temperature | `TEMPERATURE_INDEX` | `TEMPERATURE_UNIT` | `temperature` | 
| Tide coefficient | `TIDE_COEFF_INDEX` | N/A | `coeff_maree` | 


### Physico-chemical Data Files

Physico-chemical data is stored in a single tabular (CSV) file. Each line in this file matches a single measurement event (possibily including several different parameters). The meaning of each column is specified in a configuration file (cf. [Physico-chemical Data Table Configuration Files](#pysico-chemical-data-table-configuration-files). Moreover, the file may contain any number of header lines which will be skipped on import (cf  below).

A basic physico-chemical data table may look as follows

 | // Example table. Columns : date, time, site, station code, depth code, depth, surface temp., oxygen, salinity |        |             |    |         |     |      |    |      |
 |----------------------------------------------------------------------------------------------------------------|--------|-------------|----|---------|-----|------|----|------|
 | 2009-07-01                                                                                                     |13:10:00|Roscoff Astan| RA | Surface | 2.0 | 12.0 | 9999 |35.234|
 | 2009-07-02                                                                                                     |09:30:00|Roscoff Astan| RA | Surface | 2.0 | 10.0 | 9999 |34.631|
 | 2009-07-03                                                                                                     |21:40:00|Roscoff Astan| RA | Surface | 2.0 | 11.0 | 9999 |35.197|

This file contains one header line, and 3 data lines.

### Pysico-chemical Data Table Configuration Files

As for metabarcode data, these files are JSON-formatted dictionaries. A data table configuration file matching the above file would look like:

    { 
      "CAMPAIGN":"Astan 2009-2016",
      "HEADER_LINES" : 1,
      "DATE_INDEX" : 0,
      "DATE_FORMAT" : "%Y-%m-%d",
      "TIME_INDEX" : 1,
      "STATION_CODE_INDEX": 2,
      "SITENAME_INDEX": 3,
      "DEPTH_CODE_INDEX" : 4,
      "DEPTH_INDEX": 5,
      "TEMPERATURE_INDEX" : 6,
      "TEMPERATURE_UNIT" : "°C",
      "OXYGEN_INDEX" : 7,
      "OXYGEN_UNIT" : "ml/l",
      "SALINITY_INDEX" : 8,
      "SALINITY_UNIT" : "PSU",
      "DEFAULT_MISSING_VALUES": 9999,
      "DELIMITER" : ",",
      "CUSTOM_FIELDS" : []
}

### The following entries are used to describe the data table contents

#### Mandatory entries
- `CAMPAIGN` : a name that will be used to find matching metabarcode datasets,
- `HEADER_LINES`: the number of lines at the beginning of the file that must be skipped before the first line with actual data.
- `SITENAME_INDEX`: the column containing an arbitrary name for the sampling location.
- `STATION_CODE_INDEX`: the column containing the name of the sampling station, used to lookup matching metabarcode datasets.
- `DATE_INDEX`:  the column containing the date inforomation.
- `DATE_FORMAT`: a string describing how the date is formatted in the `DATE_INDEX`column. Any format compatible with [Python's strftime() function](https://docs.python.org/3/library/datetime.html#strftime-and-strptime-behavior) can be used.
- `DEPTH_INDEX`: the column containing the numerical value of the depth at which the measurement was performed.
- `DEPTH_CODE_INDEX`: the column containing the depth code at which the measurement was performed.
- `DEFAULT_MISSING_VALUES`: value used to denote that a measurement has an incorrect value.
- `DELIMITER`: the character used to separate columns in the data file.

#### Optional entries
- `TEMPERATURE_INDEX`: the column containing the value of the temperature measurements.
- ̀`TEMPERATURE_UNIT`: the measurement unit for the temperature value.
- `OXYGEN_INDEX`: the column containing oxygen the value of the oxygen measurements.
- `OXYGEN_UNIT`: the unit for the oxygen value.
- `SALINITY_INDEX`: the column containing oxygen the value of the salinity measurements.
- `SALINITY_UNIT`: the unit for the salinity value.
- `NH4_INDEX`: the column containing valus for NH4 measurements.
- `NH4_UNIT`: the unit for NH4 measurements.
- `NO2_INDEX`: the column containing valus for NO2 measurements.
- `NO2_UNIT`: the unit for NO2 measurements.
- `NO3_INDEX`: the column containing valus for NO3 measurements.
- `NO3_UNIT`: the unit for NO3 measurements.
- `PO4_INDEX`: the column containing valus for PO4 measurements.
- `PO4_UNIT`: the unit for PO4 measurements.
- `PH_INDEX`: the column containing valus for pH measurements.
- `PH_UNIT`: the unit for pH measurements.
- `CHLA_INDEX`: the column containing valus for Chlorophyll A measurements.
- `CHLA_UNIT`: the unit for Chlorophyll A measurements.
- `TIDE_COEFF_INDEX`: the column containing valus for the tide coefficient.
- `CUSTOM_FIELDS`: a list of dictionaries describing measurement parameters that are not part of the standard physico-chemical parameters.

#### Defining custom measurement parameters

Each dictionary of the `CUSTOM_FIELDS` list, must have the following entries:

- `name`: the name of the custom parameter.
- `index`: the column containing the values for the custom parameter.
- `unit`: the unit in which the values are expressed.

And may contain an optional entry:
- `missing_values`: a specific value used in the data file to denote a missing value, if not specified for a custom parameter, the global `DEFAULT_MISSING_VALUES` will be used.

Defining a custom field to store suspended matter could be done as follows:

```
CUSTOM_FIELDS : [{ 'name': 'MES', 'index': 56, 'unit': 'mg/l', 'missing_values': -99999})
```



### Associating Physico-chemical measurements to Metabarcode datasets

When loading physico-chemical data, an event is built for each line of the table using the following information :
- Sampling date,
- Campaign name,
- Station code,
- Depth code.

If a Metabarcode dataset event exists with the same event parameters, the physico-chemical measurements are associated to the matching metabarcode sampling event.
Otherwise it will be ignored.









